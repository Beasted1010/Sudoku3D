Sudoku has 3 primary features.

Create a Puzzle
Solve a Puzzle
Play Sudoku!

Create a Puzzle will create a puzzle with the difficulty that you tell it.
The Puzzle created will be saved in "Puzzle.txt"
A solution for the puzzle will also be generated

Solve a Puzzle will solve the puzzle in the "Solution.txt" file and update the file with a solution on bottom.
The format for this file is not picky. It only expects 81 numbers to be placed consecutively (0s for empty cells).
When editing this file be sure the 81 numbers representing your puzzle are the first 81 numbers in the file.

Play Sudoku! will allow the user to play Sudoku in the terminal window.
When ingame, entering 0 for Row and Col will present you with Game Options (Such as save/settings/new game/quit/ect.)
Play Sudoku has a save/load feature.
Currently only 1 save is allowed, but closing the app will NOT delete the save.



Please Note the Following:

With EXTREME difficulty, the puzzle MAY have more than one solution.
Create a Puzzle's solution may not be unique.

It is important to not delete any of the configuration files.
Perhaps in a later release I will provide permissions (restrictions) on the files