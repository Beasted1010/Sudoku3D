
INC=inc
SRC=src
OBJ=obj

CC=gcc
CFLAGS = -Wall -Iinc -I"C:\Users\die20\Documents\Code\Libraries\FYEngine\inc"
LINKFLAGS = -L"C:\Users\die20\Documents\Code\Libraries\FYEngine\lib"
LIBRARIES = -lfyengine -lgdi32 -lwinmm

main = sudoku.c

OUT = run_sudoku

_DEPS = board.h config.h solver.h sudoku_game.h userinterface.h
DEPS = $(patsubst %,$(INC)/%,$(_DEPS))
SOURCES = $(SRC)/$(main) $(subst .h,.c,$(patsubst %,$(SRC)/%,$(_DEPS)))
OBJECTS = $(OBJ)/$(subst .c,.o,$(main)) $(subst .h,.o,$(patsubst %,$(OBJ)/%,$(_DEPS)))

$(OBJ)/%.o: $(SRC)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(OUT): $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(LINKFLAGS) $(LIBRARIES)

help:
	echo sources = $(SOURCES)
	echo objects = $(OBJECTS)
	echo command = $(CC) $(CFLAGS) $(LINKFLAGS) $(LIBRARIES)

clean:
	del /q $(OBJ)\*

