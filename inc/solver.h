

#ifndef SOLVER_H
#define SOLVER_H


int ValidNum( int puzzle[][9], int num, int row, int col );
int SudokuSolver( int puzzle[][9], int row, int col );
int SolvedPuzzle( int puzzle[][9] );



#endif //SOLVER_H
