

#ifndef SUDOKU_GAME_H
#define SUDOKU_GAME_H


#include <stdio.h>


// NOTE: YOU CAN'T MODIFY A char* !!!!!!!!!!!!
extern const char* puzzleConfigFile;
extern const char* solverConfigFile;
extern const char* savesConfigFile;
extern const char* settingsConfigFile;

extern char path[];


void PlaySudoku( Board* board, FILE* fp );







#endif //SUDOKU_GAME_H
