

#ifndef CONFIG_H
#define CONFIG_H

#include "board.h"

void ZeroInitConfig( FILE* fp );
void InitConfig( int puzzle[][9], FILE* fp );

int ReadPuzzle( int puzzle[][9], FILE* fp );
void UpdateConfig( int puzzle[][9], FILE* fp );

int WriteSettingsConfig( Board* board, FILE* fp, const char* filename );
int ReadSettingsConfig( Board* board, FILE* fp );

int ValidConfigFile( const char fileName[] );

void WriteSave( int origPuzzle[][9], int puzzle[][9], FILE* fp);
void ReadSave( int origPuzzle[][9], int puzzle[][9], FILE* fp);

FILE* OpenConfigFile( const char fileName[], char* fileMode );
void CloseConfigFile( FILE* file );



#endif //CONFIG_H
