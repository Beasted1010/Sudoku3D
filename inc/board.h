
#ifndef BOARD_H
#define BOARD_H

#include "userinterface.h"

HANDLE hStdout;

// TODO: Do thorough testing on the difficulties
typedef enum DifficultyEnum
{
    EASY = 5,
    MEDIUM = 6,
    HARD = 7,
    EXTREME = 8
} Difficulty;

typedef enum StateEnum
{
    Quit, // 0
    Play  // 1
} State;

typedef enum ColorEnum
{
    WHITE, // 0
    BLACK, // 1
    RED,   // 2
    GREEN, // 3
    BLUE,  // 4
    YELLOW // 5
} Color;

typedef enum PlaneEnum
{
    BACKGROUND, // 0
    FOREGROUND  // 1
} Plane;

typedef enum ToggleEnum
{
    OFF, // 0
    ON   // 1
} Toggle;

typedef struct SettingsStruct
{
    int textSize;
    Color appTextColor;
    Color appBackgroundColor;
    Color puzzleCoordsColor;
    Color puzzleFixedNumColor;
    Color puzzleVersatileNumColor;
    Toggle handicap;

    char music[256];

    Difficulty difficulty;

    int numSettings;
} Settings;


typedef struct BoardStruct
{
    int puzzle[9][9];
    Settings settings;
    State state;
} Board;


void InitializeBoard( Board* board );
void InitializeSettings( Board* board );
void EmptyPuzzle( int puzzle[][9] );

void InitializePuzzle( int origPuzzle[][9] );
int CreatePuzzle( int origPuzzle[][9], Difficulty difficulty );
void CopyPuzzle( int dstPuzzle[][9], int srcPuzzle[][9] );
void PrintColoredPuzzle( int origPuzzle[][9], Board* board);
void PrintMonochromePuzzle( int puzzle[][9] );
void PrintPuzzle( int puzzle[][9] );


// TODO: Default case should be something other than one of the valid values...?
char* GetColor( int color );
int GetNumByColor( char* color );
char* GetToggle( int toggleValue );
char* GetDifficulty( int difficulty );

void SetConsoleColor( const Color color, Plane plane );

int TestValueByComparison( int puzzle[][9], int row, int col, int val );
int TestValueByPlugIn( int origPuzzle[][9], int puzzle[][9], int row, int col, int val );
void PlaceValue( int puzzle[][9], int row, int col, int val );

int PuzzleFilled( int puzzle[][9] );



#endif //BOARD_H
