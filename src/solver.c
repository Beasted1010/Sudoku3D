


#include "solver.h"
#include <stdio.h>


int SolvedPuzzle( int puzzle[][9] )
{
    int blockRow;
    int blockCol;

    int row1;
    int row2;
    int col1;
    int col2;

    int num;

    for( int i = 0; i < 9; i++ )
    {
        for( int j = 0; j < 9; j++ )
        {
            num = puzzle[i][j];

            blockRow = 3 * (i / 3);
            blockCol = 3 * (j / 3);

            row1 = (i + 2) % 3;
            row2 = (i + 4) % 3;
            col1 = (j + 2) % 3;
            col2 = (j + 4) % 3;


            // Check row/col
            for( int k = 0; k < 9; k++ )
            {
                // We want to compare num to everything ELSE in the column
                if( k != i )
                { if( puzzle[k][j] == num ) { return 0; } }

                // We want to compare num to everything ELSE in the row
                if( k != j )
                { if( puzzle[i][k] == num ) { return 0; } }
            }

            // There still are 4 cells to check after the row/col from their intersection
            if( puzzle[blockRow + row1][blockCol + col1] == num ) { return 0; }
            if( puzzle[blockRow + row2][blockCol + col1] == num ) { return 0; }
            if( puzzle[blockRow + row1][blockCol + col2] == num ) { return 0; }
            if( puzzle[blockRow + row2][blockCol + col2] == num ) { return 0; }
        }
    }
    return 1;
}

int ValidNum( int puzzle[][9], int num, int row, int col )
{
    int blockRow = 3 * (row / 3);
    int blockCol = 3 * (col / 3);

    int row1 = (row + 2) % 3;
    int row2 = (row + 4) % 3;
    int col1 = (col + 2) % 3;
    int col2 = (col + 4) % 3;


    // Check row/col
    for( int i = 0; i < 9; i++ )
    {
        if( puzzle[i][col] == num ) { return 0; }
        if( puzzle[row][i] == num ) { return 0; }
    }

    // There still are 4 cells to check after the row/col from their intersection
    if( puzzle[blockRow + row1][blockCol + col1] == num ) { return 0; }
    if( puzzle[blockRow + row2][blockCol + col1] == num ) { return 0; }
    if( puzzle[blockRow + row1][blockCol + col2] == num ) { return 0; }
    if( puzzle[blockRow + row2][blockCol + col2] == num ) { return 0; }

    return 1;
}

// NOTE: When called after entering a value into puzzle to be tested for solvability, if the value inserted is the
//same as a value in the row, col, and block then the incorrect value goes unnoticed (puzzle is still solvable...)
// TODO: FIX THE ABOVE.....?

// This function assumes that the cells already filled are valid entries (and thus ignores them)
int SudokuSolver( int puzzle[][9], int row, int col )
{
    // If we made it to "row 9" then we have covered the entire puzzle
    if( row == 9 ) { return 1; }

    // If a value already exists at that cell
    if( puzzle[row][col] )
    {
        // If on last column, then move down a row
        if( col == 8 )
        {
            if( SudokuSolver( puzzle, row + 1, 0 ) ) { return 1; }
        }
        else
        {
            if( SudokuSolver( puzzle, row, col + 1 ) ) { return 1; }
        }

        return 0;
    }


    for( int num = 1; num < 10; num++ )
    {
        // Check validity of our current number
        if( ValidNum( puzzle, num, row, col ) )
        {
            // Got a valid number, add it to the puzzle
            puzzle[row][col] = num;

            // If on last column, then move down a row
            if( col == 8 )
            {
                if( SudokuSolver( puzzle, row + 1, 0 ) ) { return 1; }
            }
            else
            {
                if( SudokuSolver( puzzle, row, col + 1 ) ) { return 1; }
            }

            // There is no valid value here yet
            puzzle[row][col] = 0;
        }
    }

    return 0;
}
