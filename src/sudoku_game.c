


#include <windows.h>
#include <wincon.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>

#include "board.h"
#include "config.h"
#include "solver.h"
#include "userinterface.h"


#include "sudoku_game.h"


// NOTE: YOU CAN'T MODIFY A char* !!!!!!!!!!!!
const char* puzzleConfigFile = "puzzle.txt";
const char* solverConfigFile = "solution.txt";
const char* savesConfigFile = "saves.txt";
const char* settingsConfigFile = "settings.txt";

char path[] = "rsc\\";


static inline void GameLoop( Board* board, int origPuzzle[][9] )
{
    board->state = Play;

    int row, col, val;
    int choice;

    char choiceC;

	// Characters correlating to row, col, val to test for number input
	char rowC, colC, valC;

    char* currentOption; //Was used for settings choice but reconsidered atm

    // Initialize to something other than 0
    int result = 1;
    int valid = 1;

    // Game loop
    while( board->state )
    {
        system("cls");
        choice = val = col = row = 0;
        PrintColoredPuzzle( origPuzzle, board );

        if( PuzzleFilled( board->puzzle ) )
        {
            if( SolvedPuzzle( board->puzzle ) )
            {
                system("cls");
                PrintMonochromePuzzle( board->puzzle );
                printf("SOLVED!\n\n");
                break;
            }
            printf("This board is not yet solved!\n\n");
        }

        if( !valid )
        {
            printf("1-9 are the only valid entries (0 is an exception). Please try again.\n");
            printf("A cell value entry of 0 empties a modified cell.\n");
            printf("A row and column entry of 0 will activate game options.\n");
        }
        else if( result == -1 )
        {
            printf("That value does not belong there.\n");
        }
        else if( result == -2 )
        {
            printf("This cell is a part of the original puzzle. Please try again.\n");
        }
        else if( !result )
        {
            printf("A value is already in this cell.\n");
        }

        fflush(stdin);
        printf("\nEnter in the cell (R)ow you wish to modify: ");
        scanf("%c", &rowC);

        if( rowC < '0' || rowC > '9' )
        { valid = 0; continue; }
		else
		{ row = rowC - '0'; }

        fflush(stdin);
        printf("\nEnter in the cell (C)olumn you wish to modify: ");
        scanf("%c", &colC);

        if( colC < '0' || colC > '9' )
        { valid = 0; continue; }
		else
		{ col = colC - '0'; }

        if( row == 0 && col == 0 )
        {
            printf("\nGame options activated!\n\n");
            printf("Please select from the below options.\n");
            printf("1 > Resume Game\n");
            printf("2 > New Game\n");
            printf("3 > Save Game\n");
            printf("4 > Load Game\n");
            printf("5 > Modify Settings\n");
            printf("6 > Quit Game\n");

            while( choice < 1 || choice > 6 )
            {
                fflush(stdin);
                printf("Your Choice (1-6): ");
                scanf("%c", &choiceC);
                choice = choiceC - '0';
            }

            switch( choice )
            {
                case 1: // Resume Game
                { continue; } break;

                case 2: // New Game
                {
                    result = 1;
                    valid = 1;

                    EmptyPuzzle( board->puzzle );
                    EmptyPuzzle( origPuzzle );

                    CreatePuzzle( board->puzzle, board->settings.difficulty );

                    CopyPuzzle( origPuzzle, board->puzzle );
                    continue;
                } break;

                case 3: // Save Game
                {
                    FILE* fp = OpenConfigFile( savesConfigFile, "w" );
                    WriteSave( origPuzzle, board->puzzle, fp );
                    CloseConfigFile( fp );

                    printf("\nThe puzzle has been saved!\n\n");

                    printf("Press any key to continue");
                    getch();
                    continue;
                } break;

                case 4: // Load Game
                {
                    result = 1;
                    valid = 1;

                    FILE* fp = OpenConfigFile( savesConfigFile, "r" );

                    EmptyPuzzle( board->puzzle );
                    EmptyPuzzle( origPuzzle );

                    ReadSave( origPuzzle, board->puzzle, fp );

                    CloseConfigFile( fp );
                    continue;
                } break;

                case 5: // Modify Settings
                {
                    choice = 0;
                    printf("\n");

                    printf("Select the setting to modify: \n");
                    printf("1 > Music\n");
                    printf("2 > Handicap\n");
                    printf("3 > Puzzle Versatile Number Color\n");
                    printf("4 > Puzzle Fixed Number Color\n");
                    printf("5 > Puzzle Coordinate Color\n");
                    printf("6 > App Background Color\n");
                    printf("7 > App Text Color\n");
                    printf("8 > App Text Size\n");
                    printf("9 > Exit Settings\n");

                    while( choice < 1 || choice > 9 )
                    {
                        fflush(stdin);
                        printf("Your Choice (1-9): ");
                        scanf("%c", &choiceC);
                        choice = choiceC - '0';
                    }

                    switch( choice )
                    {
                        case 1:
                        {
                            char prevSound[128];
                            char music[256];
                            char response = ' ';
                            while( response != 'Y' )
                            {
                                response = ' ';
                                system("cls");
                                choice = 0;
                                strcpy( prevSound, board->settings.music );
                                printf("Song is currently set to %s\n\n", board->settings.music);
                                printf("Select Music Setting\n");
                                printf("1 > None (mute in game music)\n");
                                printf("2 > DooblyDoo\n");
                                printf("3 > Truth in the Stones\n");

                                while( choice != 1 && choice != 2 && choice != 3 )
                                {
                                    fflush(stdin);
                                    printf("Your Choice (1,2,3): ");
                                    scanf("%c", &choiceC);
                                    choice = choiceC - '0';
                                }

                                switch( choice )
                                {
                                    case 1:
                                    { strcpy(board->settings.music, "NONE"); } break;
                                    case 2:
                                    { strcpy(board->settings.music, "DooblyDoo"); } break;
                                    case 3:
                                    { strcpy(board->settings.music, "Truth_in_the_Stones"); } break;
                                }

                                if( !strcmp(board->settings.music, "NONE") )
                                { PlaySoundA( NULL, NULL, SND_LOOP | SND_ASYNC ); }
                                else if( strcmp(board->settings.music, prevSound) )
                                {
                                    strcpy( music, path );
                                    strcat( music, board->settings.music );
                                    strcat( music, ".wav" );
                                    PlaySoundA( music, NULL, SND_LOOP | SND_ASYNC );
                                }

                                printf("\nSong is now set to %s\n", board->settings.music);

                                printf("Keep it?\n");
                                while( response != 'Y' && response != 'N' )
                                {
                                    printf("Reponse (y/n): ");
                                    response = getch(); putch(response);
                                    response = toupper(response);
                                    putch('\n');
                                }
                            }
                            putch('\n');
                        } break;

                        case 2:
                        {
                            choice = 0;
                            system("cls");
                            switch( board->settings.handicap )
                            {
                                case ON:
                                { currentOption = "ON"; } break;
                                case OFF:
                                { currentOption = "OFF"; } break;
                                default:
                                { printf("ERROR IN HANDICAP SETTINGS\n"); exit(0); }
                            }
                            printf("Handicap is currently set to: %s\n\n", currentOption);

                            printf("Select Handicap Setting\n");
                            printf("1 > ON\n");
                            printf("2 > OFF\n");

                            while( choice != 1 && choice != 2 )
                            {
                                fflush(stdin);
                                printf("Your Choice (1,2): ");
                                scanf("%c", &choiceC);
                                choice = choiceC - '0';
                            }

                            printf("\nHandicap Changed!\n");
                            switch( choice )
                            {
                                case 1:
                                {
                                    board->settings.handicap = ON;
                                    printf("Handicap is currently set to: ON\n\n");
                                } break;
                                case 2:
                                {
                                    board->settings.handicap = OFF;
                                    printf("Handicap is currently set to: OFF\n\n");
                                } break;
                            }
                        } break;

                        case 3:
                        {
                            system("cls");
                            choice = 0;
                            printf("This is the color of the numbers YOU ENTER\n");
                            printf("Puzzle Versatile Number Color is currently set to: %s\n",
                                            GetColor(board->settings.puzzleVersatileNumColor) );
                            printf("Select a Color Setting\n");
                            printf("1 > WHITE\n");
                            printf("2 > BLACK\n");
                            printf("3 > RED\n");
                            printf("4 > GREEN\n");
                            printf("5 > BLUE\n");
                            printf("6 > YELLOW\n");

                            while( choice < 1 || choice > 6 )
                            {
                                fflush(stdin);
                                printf("Your Choice (1-6): ");
                                scanf("%c", &choiceC);
                                choice = choiceC - '0';
                            }

                            board->settings.puzzleVersatileNumColor = GetNumByColor( GetColor( choice - 1 ) );

                            printf("\nPuzzle Versatile Number Color Changed!\n");

                            printf("Puzzle Versatile Number Color is currently set to: %s\n",
                                            GetColor(board->settings.puzzleVersatileNumColor) );


                        } break;

                        case 4:
                        {
                            system("cls");
                            choice = 0;
                            printf("This is the color of the numbers on the ORIGINAL BOARD\n");
                            printf("Puzzle Fixed Number Color is currently set to: %s\n",
                                            GetColor(board->settings.puzzleFixedNumColor) );
                            printf("Select a Color Setting\n");
                            printf("1 > WHITE\n");
                            printf("2 > BLACK\n");
                            printf("3 > RED\n");
                            printf("4 > GREEN\n");
                            printf("5 > BLUE\n");
                            printf("6 > YELLOW\n");

                            while( choice < 1 || choice > 6 )
                            {
                                fflush(stdin);
                                printf("Your Choice (1-6): ");
                                scanf("%c", &choiceC);
                                choice = choiceC - '0';
                            }

                            board->settings.puzzleFixedNumColor = GetNumByColor( GetColor( choice - 1 ) );

                            printf("\nPuzzle Fixed Number Color Changed!\n");

                            printf("Puzzle Fixed Number Color is currently set to: %s\n",
                                            GetColor(board->settings.puzzleFixedNumColor) );
                        } break;

                        case 5:
                        {
                            system("cls");
                            choice = 0;
                            printf("This is the color of the numbers indicating a cells position\n");
                            printf("Puzzle Coordinate Color is currently set to: %s\n",
                                            GetColor(board->settings.puzzleCoordsColor) );
                            printf("Select a Color Setting\n");
                            printf("1 > WHITE\n");
                            printf("2 > BLACK\n");
                            printf("3 > RED\n");
                            printf("4 > GREEN\n");
                            printf("5 > BLUE\n");
                            printf("6 > YELLOW\n");

                            while( choice < 1 || choice > 6 )
                            {
                                fflush(stdin);
                                printf("Your Choice (1-6): ");
                                scanf("%c", &choiceC);
                                choice = choiceC - '0';
                            }

                            board->settings.puzzleCoordsColor = GetNumByColor( GetColor( choice - 1 ) );

                            printf("\nPuzzle Coordinate Color Changed!\n");

                            printf("Puzzle Coordinate Color is currently set to: %s\n",
                                            GetColor(board->settings.puzzleCoordsColor) );
                        } break;

                        case 6:
                        {
                            system("cls");
                            printf("This is currently under construction...\n");
                            /*choice = 0;
                            printf("This is the color of the background of the app\n");
                            printf("App Background Color is currently set to: %s\n",
                                            GetColor(board->settings.appBackgroundColor) );
                            printf("Select a Color Setting\n");
                            printf("1 > WHITE\n");
                            printf("2 > BLACK\n");
                            printf("3 > RED\n");
                            printf("4 > GREEN\n");
                            printf("5 > BLUE\n");
                            printf("6 > YELLOW\n");

                            while( choice < 1 || choice > 6 )
                            {
                                fflush(stdin);
                                printf("Your Choice (1-6): ");
                                scanf("%c", &choiceC);
                                choice = choiceC - '0';
                            }

                            board->settings.appBackgroundColor = GetNumByColor( GetColor( choice - 1 ) );

                            printf("\nApp Background Color Changed!\n");

                            printf("App Background Color is currently set to: %s\n",
                                            GetColor(board->settings.appBackgroundColor) );*/
                        } break;

                        case 7:
                        {
                            system("cls");
                            choice = 0;
                            printf("This is the color of text that is not puzzle numbers\n");
                            printf("App Text Color is currently set to: %s\n",
                                            GetColor(board->settings.appTextColor) );
                            printf("Select a Color Setting\n");
                            printf("1 > WHITE\n");
                            printf("2 > BLACK\n");
                            printf("3 > RED\n");
                            printf("4 > GREEN\n");
                            printf("5 > BLUE\n");
                            printf("6 > YELLOW\n");

                            while( choice < 1 || choice > 6 )
                            {
                                fflush(stdin);
                                printf("Your Choice (1-6): ");
                                scanf("%c", &choiceC);
                                choice = choiceC - '0';
                            }

                            board->settings.appTextColor = GetNumByColor( GetColor( choice - 1 ) );

                            printf("\nApp Text Color Changed!\n");

                            printf("App Text Color is currently set to: %s\n",
                                            GetColor(board->settings.appTextColor) );
                        } break;

                        case 8:
                        {
                            system("cls");
                            printf("This is currently under construction...\n");
                        } break;

                        case 9:
                        {
                            system("cls");
                        } break;


                    }

                    printf("Press any key to continue");
                    getch();

                    FILE* sfp = OpenConfigFile( settingsConfigFile, "r+" );
                    WriteSettingsConfig( board, sfp, settingsConfigFile );
                    CloseConfigFile( sfp );

                    continue;
                } break;

                case 6: // Quit Game
                {
                    board->state = Quit;
                    printf("Quitting game...\n");
                    continue;
                } break;
            }
        }

        fflush(stdin);
        printf("\nEnter in the value you wish to add: ");
        scanf("%c", &valC);

		if( valC < '0' || valC > '9' )
        { valid = 0; continue; }
		else
		{ val = valC - '0'; }

        if( (row > 0 && row < 10) && (col > 0 && col < 10) && (val >= 0 && val < 10) )
        { valid = 1; }
        else
        { valid = 0; continue; }

        // Change row/col to be 0 indexed
        row--;
        col--;

        if( origPuzzle[row][col] )
        { result = -2; }
        else if( board->puzzle[row][col] && board->settings.handicap && val != 0 )
        { result = 0; }
        else if( !TestValueByPlugIn(origPuzzle, board->puzzle, row, col, val) && board->settings.handicap )
        { result = -1; }
        else
        { board->puzzle[row][col] = val; result = 1; }
    }
}

void PlaySudoku( Board* board, FILE* fp )
{
    switch( DisplayGameMenu() )
    {
        // New Game
        case 1:
        {
            CreatePuzzle( board->puzzle, board->settings.difficulty );

            int origPuzzle[9][9];
            CopyPuzzle( origPuzzle, board->puzzle );

            GameLoop( board, origPuzzle );
        } break;

        // Load Game
        case 2:
        {
            if( !ValidConfigFile( savesConfigFile ) )
            {
                printf("Please Check \"%s\" Configuration File.\n", savesConfigFile);
                printf("Terminating Program!\n");
                exit(0);
            }

            //ReadPuzzle( board->puzzle, fp );
            int origPuzzle[9][9];
            ReadSave( origPuzzle, board->puzzle, fp );

            GameLoop( board, origPuzzle );
        } break;

        // Settings
        case 3:
        {
            int choice;
            char choiceC;

            printf("Select the setting that you wish to change\n");
            printf("1 > Music\n");
            printf("2 > Difficulty\n");
            printf("3 > Handicap\n");
            printf("4 > Puzzle Versatile Number Color\n");
            printf("5 > Puzzle Fixed Number Color\n");
            printf("6 > Puzzle Coordinate Color\n");
            printf("7 > App Background Color\n");
            printf("8 > App Text Color\n");
            printf("9 > App Text Size\n");
            printf("10> Exit Settings\n");
            printf("Choice: ");

            while( choice < 1 || choice > 10 )
            {
				fflush(stdin);
                printf("Your Choice (1-10): ");
                scanf("%c", &choiceC);
                choice = choiceC - '0';
            }

            switch( choice )
            {
                case 1:
                {
                    char prevSound[128];
                    char music[256];
                    char response = ' ';
                    while( response != 'Y' )
                    {
                        response = ' ';
                        system("cls");
                        choice = 0;
                        strcpy( prevSound, board->settings.music );
                        printf("Song is currently set to %s\n\n", board->settings.music);
                        printf("Select Music Setting\n");
                        printf("1 > None (mute in game music)\n");
                        printf("2 > DooblyDoo\n");
                        printf("3 > Truth in the Stones\n");

                        while( choice != 1 && choice != 2 && choice != 3 )
                        {
							fflush(stdin);
                            printf("Your Choice (1,2,3): ");
                            scanf("%c", &choiceC);
                            choice = choiceC - '0';
                        }

                        switch( choice )
                        {
                            case 1:
                            { strcpy(board->settings.music, "NONE"); } break;
                            case 2:
                            { strcpy(board->settings.music, "DooblyDoo"); } break;
                            case 3:
                            { strcpy(board->settings.music, "Truth_in_the_Stones"); } break;
                        }

                        if( !strcmp(board->settings.music, "NONE") )
                        { PlaySoundA( NULL, NULL, SND_LOOP | SND_ASYNC ); }
                        else if( strcmp(board->settings.music, prevSound) )
                        {
                            strcpy( music, path );
                            strcat( music, board->settings.music );
                            strcat( music, ".wav" );
                            PlaySoundA( music, NULL, SND_LOOP | SND_ASYNC );
                        }

                        printf("\nSong is now set to %s\n\n", board->settings.music);

                        printf("Keep it?\n");
                        while( response != 'Y' && response != 'N' )
                        {
                            printf("Reponse (y/n): ");
                            response = getch(); putch(response);
                            response = toupper(response);
                            putch('\n');
                        }
                    }
                    putch('\n');
                }break;

                case 2:
                {
                    system("cls");
                    choice = 0;
                    char* difficulty;
                    switch( board->settings.difficulty )
                    {
                        case EASY:
                        { difficulty = "EASY"; } break;
                        case MEDIUM:
                        { difficulty = "MEDIUM"; } break;
                        case HARD:
                        { difficulty = "HARD"; } break;
                        case EXTREME:
                        { difficulty = "EXTREME"; } break;
                        default:
                        { printf("ERROR IN DIFFICULTY SETTINGS\n"); exit(0); }
                    }
                    printf("Difficulty is currently set to: %s\n\n", difficulty);


                    printf("Select Difficulty\n");
                    printf("1 > EASY\n");
                    printf("2 > MEDIUM\n");
                    printf("3 > HARD\n");
                    printf("4 > EXTREME\n");

                    while( choice != 1 && choice != 2 && choice != 3 && choice != 4 )
                    {
						fflush(stdin);
                        printf("Your Choice (1,2,3,4): ");
                        scanf("%c", &choiceC);
                        choice = choiceC - '0';
                    }

                    printf("\nDifficulty Changed!\n");
                    switch( choice )
                    {
                        case 1:
                        {
                            board->settings.difficulty = EASY;
                            printf("Difficulty is currently set to EASY\n\n");
                        } break;
                        case 2:
                        {
                            board->settings.difficulty = MEDIUM;
                            printf("Difficulty is currently set to MEDIUM\n\n");
                        } break;
                        case 3:
                        {
                            board->settings.difficulty = HARD;
                            printf("Difficulty is currently set to HARD\n\n");
                        } break;
                        case 4:
                        {
                            board->settings.difficulty = EXTREME;
                            printf("Difficulty is currently set to EXTREME\n\n");
                        } break;
                    }
                } break;

                case 3:
                {
                    system("cls");
                    choice = 0;
                    char* handicap;
                    switch( board->settings.handicap )
                    {
                        case ON:
                        { handicap = "ON"; } break;
                        case OFF:
                        { handicap = "OFF"; } break;
                        default:
                        { printf("ERROR IN HANDICAP SETTINGS\n"); exit(0); }
                    }
                    printf("Handicap is currently set to: %s\n\n", handicap);

                    printf("Select Handicap Setting\n");
                    printf("1 > ON\n");
                    printf("2 > OFF\n");

                    while( choice != 1 && choice != 2 )
                    {
						fflush(stdin);
                        printf("Your Choice (1,2): ");
                        scanf("%c", &choiceC);
                        choice = choiceC - '0';
                    }

                    printf("\nHandicap Changed!\n");
                    switch( choice )
                    {
                        case 1:
                        {
                            board->settings.handicap = ON;
                            printf("Handicap is currently set to: ON\n\n");
                        } break;
                        case 2:
                        {
                            board->settings.handicap = OFF;
                            printf("Handicap is currently set to: OFF\n\n");
                        } break;
                    }
                } break;

                case 4:
                {
                    system("cls");
                    choice = 0;
                    printf("This is the color of the numbers YOU ENTER\n");
                    printf("Puzzle Versatile Number Color is currently set to: %s\n",
                                    GetColor(board->settings.puzzleVersatileNumColor) );
                    printf("Select a Color Setting\n");
                    printf("1 > WHITE\n");
                    printf("2 > BLACK\n");
                    printf("3 > RED\n");
                    printf("4 > GREEN\n");
                    printf("5 > BLUE\n");
                    printf("6 > YELLOW\n");

                    while( choice < 1 || choice > 6 )
                    {
						fflush(stdin);
                        printf("Your Choice (1-6): ");
                        scanf("%c", &choiceC);
                        choice = choiceC - '0';
                    }

                    board->settings.puzzleVersatileNumColor = GetNumByColor( GetColor( choice - 1 ) );

                    printf("\nPuzzle Versatile Number Color Changed!\n");

                    printf("Puzzle Versatile Number Color is currently set to: %s\n",
                                    GetColor(board->settings.puzzleVersatileNumColor) );


                } break;

                case 5:
                {
                    system("cls");
                    choice = 0;
                    printf("This is the color of the numbers on the ORIGINAL BOARD\n");
                    printf("Puzzle Fixed Number Color is currently set to: %s\n",
                                    GetColor(board->settings.puzzleFixedNumColor) );
                    printf("Select a Color Setting\n");
                    printf("1 > WHITE\n");
                    printf("2 > BLACK\n");
                    printf("3 > RED\n");
                    printf("4 > GREEN\n");
                    printf("5 > BLUE\n");
                    printf("6 > YELLOW\n");

                    while( choice < 1 || choice > 6 )
                    {
						fflush(stdin);
                        printf("Your Choice (1-6): ");
                        scanf("%c", &choiceC);
                        choice = choiceC - '0';
                    }

                    board->settings.puzzleFixedNumColor = GetNumByColor( GetColor( choice - 1 ) );

                    printf("\nPuzzle Fixed Number Color Changed!\n");

                    printf("Puzzle Fixed Number Color is currently set to: %s\n",
                                    GetColor(board->settings.puzzleFixedNumColor) );
                } break;

                case 6:
                {
                    system("cls");
                    choice = 0;
                    printf("This is the color of the numbers indicating a cells position\n");
                    printf("Puzzle Coordinate Color is currently set to: %s\n",
                                    GetColor(board->settings.puzzleCoordsColor) );
                    printf("Select a Color Setting\n");
                    printf("1 > WHITE\n");
                    printf("2 > BLACK\n");
                    printf("3 > RED\n");
                    printf("4 > GREEN\n");
                    printf("5 > BLUE\n");
                    printf("6 > YELLOW\n");

                    while( choice < 1 || choice > 6 )
                    {
						fflush(stdin);
                        printf("Your Choice (1-6): ");
                        scanf("%c", &choiceC);
                        choice = choiceC - '0';
                    }

                    board->settings.puzzleCoordsColor = GetNumByColor( GetColor( choice - 1 ) );

                    printf("\nPuzzle Coordinate Color Changed!\n");

                    printf("Puzzle Coordinate Color is currently set to: %s\n",
                                    GetColor(board->settings.puzzleCoordsColor) );
                } break;

                case 7:
                {
                    system("cls");
                    printf("This is currently under construction...\n");
                    /*choice = 0;
                    printf("This is the color of the background of the app\n");
                    printf("App Background Color is currently set to: %s\n",
                                    GetColor(board->settings.appBackgroundColor) );
                    printf("Select a Color Setting\n");
                    printf("1 > WHITE\n");
                    printf("2 > BLACK\n");
                    printf("3 > RED\n");
                    printf("4 > GREEN\n");
                    printf("5 > BLUE\n");
                    printf("6 > YELLOW\n");

                    while( choice < 1 || choice > 6 )
                    {
						fflush(stdin);
                        printf("Your Choice: ");
                        scanf("%c", &choiceC);
                        choice = choiceC - '0';
                    }

                    board->settings.appBackgroundColor = GetNumByColor( GetColor( choice - 1 ) );

                    printf("\nApp Background Color Changed!\n");

                    printf("App Background Color is currently set to: %s\n",
                                    GetColor(board->settings.appBackgroundColor) );*/
                } break;

                case 8:
                {
                    system("cls");
                    choice = 0;
                    printf("This is the color of text that is not puzzle numbers\n");
                    printf("App Text Color is currently set to: %s\n",
                                    GetColor(board->settings.appTextColor) );
                    printf("Select a Color Setting\n");
                    printf("1 > WHITE\n");
                    printf("2 > BLACK\n");
                    printf("3 > RED\n");
                    printf("4 > GREEN\n");
                    printf("5 > BLUE\n");
                    printf("6 > YELLOW\n");

                    while( choice < 1 || choice > 6 )
                    {
						fflush(stdin);
                        printf("Your Choice (1-6): ");
                        scanf("%c", &choiceC);
                        choice = choiceC - '0';
                    }

                    board->settings.appTextColor = GetNumByColor( GetColor( choice - 1 ) );

                    printf("\nApp Text Color Changed!\n");

                    printf("App Text Color is currently set to: %s\n",
                                    GetColor(board->settings.appTextColor) );
                } break;

                case 9:
                {
                    system("cls");
                    printf("This is currently under construction...\n");
                } break;

                case 10:
                {
                    system("cls");
                } break;
            }

            FILE* sfp = OpenConfigFile( settingsConfigFile, "r+" );
            WriteSettingsConfig( board, sfp, settingsConfigFile );
            CloseConfigFile( sfp );
        } break;

        // Quit Game
        case 4:
        {

        } break;
    }
}
