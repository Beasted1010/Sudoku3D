

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "config.h"

FILE* OpenConfigFile( const char fileName[], char* fileMode )
{
    FILE* fp;

    char file[] = "config\\";

    strcat( file, fileName );

    const char* f = file;

    fp = fopen( f, fileMode );

    if( !fp )
    {
        printf("File (%s) did not open!\n", fileName);
        exit(0);
    }

    return fp;
}

void CloseConfigFile( FILE* file )
{
    fclose(file);
}

int ReadPuzzle( int puzzle[][9], FILE* fp )
{
    // Initialize val to something other than what EOF may be (-1, 0, 1 all seem reasonable for EOF)
    int val = 2;

    // Move to puzzle
    while( val != EOF )
    {
        val = fgetc(fp);
        if( isdigit(val) )
        {
            ungetc( val, fp );
            break;
        }
        else if( val == EOF )
        {
            return 0;
        }
    }

    // Read puzzle
    for( int i = 0; i < 9; i++ )
    {
        for( int j = 0; j < 9; j++ )
        {
            while( (val = fgetc(fp)) != EOF )
            {
                if( isdigit(val) )
                {
                    puzzle[i][j] = val - '0';
                    break;
                }
                else if( isalpha(val) )
                {
                    return 0;
                }
            }
        }
    }

    return 1;
}

int WriteSettingsConfig( Board* board, FILE* fp, const char* filename )
{
    FILE* fp2;

    fp2 = OpenConfigFile("replica.txt", "w");

    char line[512];
    char val = '0'; // Something other than '_'

    while( val != '_' )
    {
        fgets(line, sizeof(line), fp);
        val = line[0];
        fputs(line, fp2);
    }

    fprintf(fp2, "App Text Size                              |%i\n", board->settings.textSize);

    char* value = GetColor(board->settings.appTextColor);
    fprintf(fp2, "App Text Color                             |%s\n", value);

    value = GetColor(board->settings.appBackgroundColor);
    fprintf(fp2, "App Background Color                       |%s\n", value);

    value = GetColor(board->settings.puzzleCoordsColor);
    fprintf(fp2, "Puzzle Coordinate Color                    |%s\n", value);

    value = GetColor(board->settings.puzzleFixedNumColor);
    fprintf(fp2, "Puzzle Fixed Number Color                  |%s\n", value);

    value = GetColor(board->settings.puzzleVersatileNumColor);
    fprintf(fp2, "Puzzle Versatile Number Color              |%s\n", value);

    value = GetToggle(board->settings.handicap);
    fprintf(fp2, "Handicap Mode                              |%s\n", value);

    value = GetDifficulty(board->settings.difficulty);
    fprintf(fp2, "Difficulty                                 |%s\n", value);

    value = board->settings.music;
    fprintf(fp2, "Music                                      |%s\n", value);


    fclose(fp);
    fclose(fp2);

    if( remove("config\\settings.txt") )
    { printf("Error with removing configuration file.\n"); }
    if( rename("config\\replica.txt", "config\\settings.txt") )
    { printf("Error with renaming configuration file.\n"); }

    return 1;
}

int ReadSettingsConfig( Board* board, FILE* fp )
{
    // Initialize val to something other than what EOF may be (-1, 0, 1 all seem reasonable for EOF)
    int val = 2;

    char line[12][256];

    board->settings.numSettings = 0;

    while( val != EOF )
    {
        while( val != '|'  && val != EOF )
        { val = fgetc(fp); }

        fgets(line[board->settings.numSettings], sizeof(line[board->settings.numSettings]), fp);
        val = fgetc(fp);

        board->settings.numSettings++;
    }

    /*if( i != board->settings.numSettings )
    {
        printf("Error while reading settings configuration file!\n");
        printf("Terminating Program\n");
        exit(0);
    }*/

    for( int i = 0; i < board->settings.numSettings; i++ )
    {
        // Used to get rid of \n character in input
        strtok(line[i], "\n");
        switch( i )
        {
            case 0:
            {
                int size = line[i][0] - '0'; // TODO: This is BAD BAD BAD OH SO BAD
                size *= 10;
                size += (line[i][1] - '0');

                board->settings.textSize = size;
            } break;

            // TODO: Use GetColor() and GetNumByColor() to clean up the below
            case 1:
            {
                if( !strcmp(line[i], "BLUE") )
                {
                    board->settings.appTextColor = BLUE;
                }
                else if( !strcmp(line[i], "GREEN") )
                {
                    board->settings.appTextColor = GREEN;
                }
                else if( !strcmp(line[i], "RED") )
                {
                    board->settings.appTextColor = RED;
                }
                else if( !strcmp(line[i], "YELLOW") )
                {
                    board->settings.appTextColor = YELLOW;
                }
                else if( !strcmp(line[i], "WHITE") )
                {
                    board->settings.appTextColor = WHITE;
                }
                else if( !strcmp(line[i], "BLACK") )
                {
                    board->settings.appTextColor = BLACK;
                }
                else
                {
                    printf("Error with App Text Color format in settings configuration file\n");
                    return 0;
                }
            } break;

            case 2:
            {
                if( !strcmp(line[i], "BLUE") )
                {
                    board->settings.appBackgroundColor = BLUE;
                }
                else if( !strcmp(line[i], "GREEN") )
                {
                    board->settings.appBackgroundColor = GREEN;
                }
                else if( !strcmp(line[i], "RED") )
                {
                    board->settings.appBackgroundColor = RED;
                }
                else if( !strcmp(line[i], "YELLOW") )
                {
                    board->settings.appTextColor = YELLOW;
                }
                else if( !strcmp(line[i], "WHITE") )
                {
                    board->settings.appBackgroundColor = WHITE;
                }
                else if( !strcmp(line[i], "BLACK") )
                {
                    board->settings.appBackgroundColor = BLACK;
                }
                else
                {
                    printf("Error with App Background Color format in settings configuration file\n");
                    return 0;
                }
            } break;

            case 3:
            {
                if( !strcmp(line[i], "BLUE") )
                {
                    board->settings.puzzleCoordsColor = BLUE;
                }
                else if( !strcmp(line[i], "GREEN") )
                {
                    board->settings.puzzleCoordsColor = GREEN;
                }
                else if( !strcmp(line[i], "RED") )
                {
                    board->settings.puzzleCoordsColor = RED;
                }
                else if( !strcmp(line[i], "YELLOW") )
                {
                    board->settings.puzzleCoordsColor = YELLOW;
                }
                else if( !strcmp(line[i], "WHITE") )
                {
                    board->settings.puzzleCoordsColor = WHITE;
                }
                else if( !strcmp(line[i], "BLACK") )
                {
                    board->settings.puzzleCoordsColor = BLACK;
                }
                else
                {
                    printf("Error with Puzzle Coordinate Color format in settings configuration file\n");
                    return 0;
                }
            } break;

            case 4:
            {
                if( !strcmp(line[i], "BLUE") )
                {
                    board->settings.puzzleFixedNumColor = BLUE;
                }
                else if( !strcmp(line[i], "GREEN") )
                {
                    board->settings.puzzleFixedNumColor = GREEN;
                }
                else if( !strcmp(line[i], "RED") )
                {
                    board->settings.puzzleFixedNumColor = RED;
                }
                else if( !strcmp(line[i], "YELLOW") )
                {
                    board->settings.puzzleFixedNumColor = YELLOW;
                }
                else if( !strcmp(line[i], "WHITE") )
                {
                    board->settings.puzzleFixedNumColor = WHITE;
                }
                else if( !strcmp(line[i], "BLACK") )
                {
                    board->settings.puzzleFixedNumColor = BLACK;
                }
                else
                {
                    printf("Error with Puzzle Fixed Number Color format in settings configuration file\n");
                    return 0;
                }
            } break;

            case 5:
            {
                if( !strcmp(line[i], "BLUE") )
                {
                    board->settings.puzzleVersatileNumColor = BLUE;
                }
                else if( !strcmp(line[i], "GREEN") )
                {
                    board->settings.puzzleVersatileNumColor = GREEN;
                }
                else if( !strcmp(line[i], "RED") )
                {
                    board->settings.puzzleVersatileNumColor = RED;
                }
                else if( !strcmp(line[i], "YELLOW") )
                {
                    board->settings.puzzleVersatileNumColor = YELLOW;
                }
                else if( !strcmp(line[i], "WHITE") )
                {
                    board->settings.puzzleVersatileNumColor = WHITE;
                }
                else if( !strcmp(line[i], "BLACK") )
                {
                    board->settings.puzzleVersatileNumColor = BLACK;
                }
                else
                {
                    printf("Error with Puzzle Versatile Number Color format in settings configuration file\n");
                    return 0;
                }
            } break;

            case 6:
            {
                if( !strcmp(line[i], "ON") )
                {
                    board->settings.handicap = ON;
                }
                else if( !strcmp(line[i], "OFF") )
                {
                    board->settings.handicap = OFF;
                }
                else
                {
                    printf("Error with Handicap Mode format in settings configuration file\n");
                    return 0;
                }
            } break;

            case 7:
            {
                if( !strcmp(line[i], "EASY") )
                {
                    board->settings.difficulty = EASY;
                }
                else if( !strcmp(line[i], "MEDIUM") )
                {
                    board->settings.difficulty = MEDIUM;
                }
                else if( !strcmp(line[i], "HARD") )
                {
                    board->settings.difficulty = HARD;
                }
                else if( !strcmp(line[i], "EXTREME") )
                {
                    board->settings.difficulty = EXTREME;
                }
                else
                {
                    printf("Error with Difficulty format in settings configuration file\n");
                    return 0;
                }
            } break;

            case 8:
            {
                strcpy( board->settings.music, line[i] );
            } break;
        }
    }
    return 1;
}

int ValidConfigFile( const char fileName[] )
{
    FILE* fp = OpenConfigFile( fileName, "r" );

    int val;

    val = fgetc(fp);

    if( val == EOF )
    {
        printf("\nConfiguration File is Empty!\n");
        return 0;
    }

    ungetc( val, fp );

    int puzzle[9][9];

    // Read in a puzzles worth of numbers
    if( !ReadPuzzle( puzzle, fp ) )
    {
        printf("\nConfiguration File Format Incorrect!!\n");
        return 0;
    }


    // See if any numbers remain, if so then the configuration file is likely invalid
    // Note that we stop if we come across a dash '-' which is where the SOLVED part is
    while( (val = fgetc(fp)) != EOF )
    {
        if( isdigit(val) || isalpha(val) )
        {
            printf("\nConfiguration File Format Incorrect!\n");
            return 0;
        }
        else if( val == '-' )
        {
            break;
        }
    }

    CloseConfigFile( fp );

    return 1;
}

void UpdateConfig( int puzzle[][9], FILE* fp )
{
    fseek(fp, 0, SEEK_CUR);

    fprintf(fp, "\n");
    fprintf(fp, "\n-----------------SOLVED-----------------\n");

    fprintf(fp, "   C  1  2  3      4  5  6      7  8  9\n");
    fprintf(fp, "R     _________________________________\n");


    for( int i = 0; i < 9; i++ )
    {
        if( i == 3 || i == 6 )
        {
            fprintf(fp, " |    ");
            for( int k = 0; k < 11; k++ )
            { fprintf(fp, "___"); }
            fprintf(fp, "\n");
        }

        fprintf(fp, "%i|   ", i + 1);

        for( int j = 0; j < 9; j++ )
        {
            if( j == 3 || j == 6 )
            { fprintf(fp, " |  "); }
            fprintf(fp, " %i ", puzzle[i][j]);
        }
        fprintf(fp, "\n");
    }
}

void ReadSave( int origPuzzle[][9], int puzzle[][9], FILE* fp)
{
    ReadPuzzle( origPuzzle, fp );
    ReadPuzzle( puzzle, fp );
}

void WriteSave( int origPuzzle[][9], int puzzle[][9], FILE* fp)
{
    fprintf(fp, "\n--------------Original Puzzle--------------\n");

    // NOTE: Commented out until I fix problem with having these new numbers in config all of a sudden
    //fprintf(fp, "   C  1  2  3      4  5  6      7  8  9\n");
    //fprintf(fp, "R     _________________________________\n");

    for( int i = 0; i < 9; i++ )
    {
        if( i == 3 || i == 6 )
        {
            //fprintf(fp, " |    ");
            for( int k = 0; k < 12; k++ )
            { fprintf(fp, "___"); }
            fprintf(fp, "\n");
        }

        //fprintf(fp, "%i|   ", i + 1);

        for( int j = 0; j < 9; j++ )
        {
            if( j == 3 || j == 6 )
            { fprintf(fp, " |  "); }

            if( origPuzzle[i][j] )
            { fprintf(fp, " %i ", origPuzzle[i][j]); }
            else
            { fprintf(fp, " 0 "); }
        }
        fprintf(fp, "\n");
    }

    fprintf(fp, "\n--------------USER SAVED POINT--------------\n");

    for( int i = 0; i < 9; i++ )
    {
        if( i == 3 || i == 6 )
        {
            //fprintf(fp, " |    ");
            for( int k = 0; k < 12; k++ )
            { fprintf(fp, "___"); }
            fprintf(fp, "\n");
        }

        //fprintf(fp, "%i|   ", i + 1);

        for( int j = 0; j < 9; j++ )
        {
            if( j == 3 || j == 6 )
            { fprintf(fp, " |  "); }

            if( puzzle[i][j] )
            { fprintf(fp, " %i ", puzzle[i][j]); }
            else
            { fprintf(fp, " 0 "); }
        }
        fprintf(fp, "\n");
    }
}

void InitConfig( int puzzle[][9], FILE* fp )
{
    fprintf(fp, "\n----------------ORIGINAL----------------\n");

    fprintf(fp, "   C  1  2  3      4  5  6      7  8  9\n");
    fprintf(fp, "R     _________________________________\n");

    for( int i = 0; i < 9; i++ )
    {
        if( i == 3 || i == 6 )
        {
            fprintf(fp, " |    ");
            for( int k = 0; k < 11; k++ )
            { fprintf(fp, "___"); }
            fprintf(fp, "\n");
        }

        fprintf(fp, "%i|   ", i + 1);

        for( int j = 0; j < 9; j++ )
        {
            if( j == 3 || j == 6 )
            { fprintf(fp, " |  "); }

            if( puzzle[i][j] )
            { fprintf(fp, " %i ", puzzle[i][j]); }
            else
            { fprintf(fp, "   "); }
        }
        fprintf(fp, "\n");
    }
}

// 0 Initializes puzzle
void ZeroInitConfig( FILE* fp )
{
    fprintf(fp, "\n----------------ORIGINAL----------------\n");

    fprintf(fp, "   C  1  2  3      4  5  6      7  8  9\n");
    fprintf(fp, "R     _________________________________\n");

    for( int i = 0; i < 9; i++ )
    {
        if( i == 3 || i == 6 )
        {
            fprintf(fp, " |    ");
            for( int k = 0; k < 11; k++ )
            { fprintf(fp, "___"); }
            fprintf(fp, "\n");
        }

        fprintf(fp, "%i|   ", i + 1);

        for( int j = 0; j < 9; j++ )
        {
            if( j == 3 || j == 6 )
            { fprintf(fp, " |  "); }
            fprintf(fp, " 0 ");
        }
        fprintf(fp, "\n");
    }
}
