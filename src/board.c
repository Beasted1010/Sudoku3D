

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <windows.h>


#include "board.h"
#include "solver.h"

// TODO: Create a ValidatePuzzle Function that will run the created puzzle against the solver. Is this needed?


// NOTE: Perhaps get rid of this, thought something here would be useful, but doesn't seem to be
void InitializeBoard( Board* board )
{
    board = malloc( sizeof(Board) );
    //...
}

void InitializeSettings( Board* board )
{
    board->settings.textSize = 20;
    board->settings.appTextColor = WHITE;
    board->settings.appBackgroundColor = BLACK;
    board->settings.puzzleCoordsColor = WHITE;
    board->settings.puzzleFixedNumColor = RED;
    board->settings.puzzleVersatileNumColor = GREEN;
    board->settings.handicap = OFF;
    board->settings.difficulty = MEDIUM;
    strcpy( board->settings.music, "NONE"); // "Truth_in_the_Stones" ); //"DooblyDoo"

    //board->settings.numSettings = 8;
}

char* GetColor( int color )
{
    switch( color )
    {
        case RED:
        { return "RED"; } break;

        case GREEN:
        { return "GREEN"; } break;

        case BLUE:
        { return "BLUE"; } break;

        case YELLOW:
        { return "YELLOW"; } break;

        case BLACK:
        { return "BLACK"; } break;

        default:
        { return "WHITE"; } break;
    }
}

/*
Using the following format:
WHITE, // 0
BLACK, // 1
RED,   // 2
GREEN, // 3
BLUE,  // 4
YELLOW // 5
*/
int GetNumByColor( char* color )
{
    if( !strcmp( color, "WHITE" ) )
    { return WHITE; }
    else if( !strcmp( color, "BLACK" ) )
    { return BLACK; }
    else if( !strcmp( color, "RED" ) )
    { return RED; }
    else if( !strcmp( color, "GREEN" ) )
    { return GREEN; }
    else if( !strcmp( color, "BLUE" ) )
    { return BLUE; }
    else if( !strcmp( color, "YELLOW" ) )
    { return YELLOW; }
    else
    { printf("Error in GetNumByColor... TERMINATING\n"); exit(0); }
}

char* GetToggle( int toggleValue )
{
    switch( toggleValue )
    {
        case OFF:
        { return "OFF"; } break;

        default:
        { return "ON"; } break;
    }
}

char* GetDifficulty( int difficulty )
{
    switch( difficulty )
    {
        case EASY:
        { return "EASY"; }

        case HARD:
        { return "HARD"; }

        case EXTREME:
        { return "EXTREME"; }

        default:
        { return "MEDIUM"; }
    }
}

void SetConsoleColor( const Color color, Plane plane )
{
    hStdout = GetStdHandle(STD_OUTPUT_HANDLE);


    //WORD saved_attributes;

    if( plane )
    {
        switch( color )
        {
            case WHITE:
            {
                SetConsoleTextAttribute( hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE |
                                                                                     FOREGROUND_INTENSITY );
            } break;

            case BLACK:
            {
                SetConsoleTextAttribute( hStdout, FOREGROUND_INTENSITY );
            } break;

            case RED:
            {
                SetConsoleTextAttribute( hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY );
            } break;

            case GREEN:
            {
                SetConsoleTextAttribute( hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY );
            } break;

            case BLUE:
            {
                SetConsoleTextAttribute( hStdout, FOREGROUND_BLUE | FOREGROUND_INTENSITY );
            } break;

            case YELLOW:
            {
                SetConsoleTextAttribute( hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY );
            } break;
        }
    }
    else
    {
        switch( color )
        {
            case WHITE:
            {
                SetConsoleTextAttribute( hStdout, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE );
            } break;

            case BLACK:
            {
                SetConsoleTextAttribute( hStdout, BACKGROUND_INTENSITY );
            } break;

            case RED:
            {
                SetConsoleTextAttribute( hStdout, BACKGROUND_RED | BACKGROUND_INTENSITY );
            } break;

            case GREEN:
            {
                SetConsoleTextAttribute( hStdout, BACKGROUND_GREEN | BACKGROUND_INTENSITY );
            } break;

            case BLUE:
            {
                SetConsoleTextAttribute( hStdout, BACKGROUND_BLUE | BACKGROUND_INTENSITY );
            } break;

            case YELLOW:
            {
                SetConsoleTextAttribute( hStdout, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_INTENSITY );
            } break;
        }
    }
    //GetConsoleScreenBufferInfo( hStdout, &consoleInfo );
    //saved_attributes = consoleInfo.wAttributes;
}

void PrintPuzzle( int puzzle[][9] )
{
    printf("  C  1 2 3     4 5 6     7 8 9\n");
    printf("R    _________________________\n");

    for( int i = 0; i < 9; i++ )
    {
        if( i == 3 || i == 6 )
        {
            printf(" |   ");
            for( int k = 0; k < 5; k++ )
            { printf("_____"); }
            printf("\n");
        }

        printf("%i|   ", i + 1);

        for( int j = 0; j < 9; j++ )
        {
            if( j == 3 || j == 6 )
            { printf(" |  "); }

            if( puzzle[i][j] )
            { printf("%i ", puzzle[i][j]); }
            else
            { printf("  "); }
        }
        printf("\n");
    }
    printf("\n");
}

void PrintColoredPuzzle( int origPuzzle[][9], Board* board)
{
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    WORD saved_attributes;

    hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

    GetConsoleScreenBufferInfo( hStdout, &consoleInfo );
    saved_attributes = consoleInfo.wAttributes;

    SetConsoleColor( board->settings.puzzleCoordsColor, FOREGROUND );
    printf("  C  1 2 3     4 5 6     7 8 9\n");
    printf("R");

    SetConsoleTextAttribute( hStdout, saved_attributes );
    printf("    _________________________\n");




    for( int i = 0; i < 9; i++ )
    {
        if( i == 3 || i == 6 )
        {
            printf(" |   ");
            for( int k = 0; k < 5; k++ )
            { printf("_____"); }
            printf("\n");
        }

        SetConsoleColor( board->settings.puzzleCoordsColor, FOREGROUND );
        printf("%i", i + 1);

        SetConsoleTextAttribute( hStdout, saved_attributes );
        printf("|   ");

        for( int j = 0; j < 9; j++ )
        {
            if( j == 3 || j == 6 )
            { printf(" |  "); }

            if( origPuzzle[i][j] )
            {
                SetConsoleColor( board->settings.puzzleFixedNumColor, FOREGROUND );
                printf("%i ", board->puzzle[i][j]);

                SetConsoleTextAttribute( hStdout, saved_attributes );
            }
            else if( board->puzzle[i][j] )
            {
                SetConsoleColor( board->settings.puzzleVersatileNumColor, FOREGROUND );
                printf("%i ", board->puzzle[i][j]);

                SetConsoleTextAttribute( hStdout, saved_attributes );
            }
            else
            { printf("  "); }
        }
        printf("\n");
    }
    printf("\n");
}

void PrintMonochromePuzzle( int puzzle[][9] )
{
    printf("  C  1 2 3     4 5 6     7 8 9\n");
    printf("R    _________________________\n");

    hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    WORD saved_attributes;

    for( int i = 0; i < 9; i++ )
    {
        if( i == 3 || i == 6 )
        {
            printf(" |   ");
            for( int k = 0; k < 5; k++ )
            { printf("_____"); }
            printf("\n");
        }

        printf("%i|   ", i + 1);

        for( int j = 0; j < 9; j++ )
        {
            if( j == 3 || j == 6 )
            { printf(" |  "); }

            if( puzzle[i][j] )
            {
                GetConsoleScreenBufferInfo( hStdout, &consoleInfo );
                saved_attributes = consoleInfo.wAttributes;

                SetConsoleTextAttribute( hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY );
                printf("%i ", puzzle[i][j]);

                SetConsoleTextAttribute( hStdout, saved_attributes );
            }
            else
            { printf("  "); }
        }
        printf("\n");
    }
    printf("\n");
}

void CopyPuzzle( int dstPuzzle[][9], int srcPuzzle[][9] )
{
    for( int i = 0; i < 9; i++ )
    {
        for( int j = 0; j < 9; j++ )
        {
            dstPuzzle[i][j] = srcPuzzle[i][j];
        }
    }
}

static inline void ZeroInitPuzzle( int puzzle[][9] )
{
    // Fill puzzle with zeroes
    for( int i = 0; i < 9; i++ )
    {
        for( int j = 0; j < 9; j++ )
        { puzzle[i][j] = 0; }
    }
}

void InitializePuzzle( int puzzle[][9] )
{
    ZeroInitPuzzle( puzzle );
}

int TestValueByComparison( int puzzle[][9], int row, int col, int val )
{
    int solvedPuzzle[9][9];

    for( int i = 0; i < 9; i++ )
    {
        for( int j = 0; j < 9; j++ )
        {
            solvedPuzzle[i][j] = puzzle[i][j];
        }
    }

    SudokuSolver( solvedPuzzle, row, col );

    PrintPuzzle( solvedPuzzle );
    int test; scanf("%i", &test);

    if( solvedPuzzle[row][col] != val )
    {
        return 0;
    }

    return 1;
}

int TestValueByPlugIn( int origPuzzle[][9], int puzzle[][9], int row, int col, int val )
{
    if( val == 0 )
    {
        puzzle[row][col] = 0;
        return 1;
    }

    int solvedPuzzle[9][9];
    int result = 1;

    for( int i = 0; i < 9; i++ )
    {
        for( int j = 0; j < 9; j++ )
        {
            solvedPuzzle[i][j] = origPuzzle[i][j];//puzzle[i][j];
        }
    }

    solvedPuzzle[row][col] = val;

    // TODO: Modify SudokuSolver if need be to return 0 when val is already in col, row, AND block
    // TODO: ValidNum is taking origPuzzle, it was taking puzzle, if this freaks out anytime, it's due to this
    if( !SudokuSolver( solvedPuzzle, 0, 0 ) || !ValidNum( origPuzzle, val, row, col ) )
    {
        result = 0;
    }

    return result;
}

void PlaceValue( int puzzle[][9], int row, int col, int val )
{
    puzzle[row][col] = val;
}

// TODO: Replace this with a different method, perhaps by use of structures and keeping track of # of empty cells
int PuzzleFilled( int puzzle[][9] )
{
    for( int i = 0; i < 9; i++ )
    {
        for( int j = 0; j < 9; j++ )
        {
            if( !puzzle[i][j] )
            { return 0; }
        }
    }

    return 1;
}

static inline void EmptySomeCells( int puzzle[][9], Difficulty difficulty )
{
    int chance;

    //int numEmpty = 0;
    int numConsec = 0;


    int row1;
    int row2;
    int col1;
    int col2;

    int blockRow;
    int blockCol;

//int temp = 0;


    for( int i = 0; i < 9; i++ )
    {
        for( int j = 0; j < 9; j++ )
        {
            blockRow = 3 * (i / 3);
            blockCol = 3 * (j / 3);

            row1 = (i + 2) % 3;
            row2 = (i + 4) % 3;
            col1 = (j + 2) % 3;
            col2 = (j + 4) % 3;

            chance = (rand() % 10) + 1;


            // IDEA: If 4 of the toher blocks are already empty, then make it less likely that this block will be
            if( puzzle[blockRow + row1][blockCol + col1] == 0 && puzzle[blockRow + row2][blockCol + col1] == 0 &&
            puzzle[blockRow + row1][blockCol + col2] == 0 && puzzle[blockRow + row2][blockCol + col2] == 0 )
            { chance += 5; }//temp = 1; }//printf("Chance: %i\n", chance); }

            if( chance < difficulty && numConsec < 5 )// && numEmpty < (difficulty * 5) ) )
            {
                puzzle[i][j] = 0;
                //numEmpty++;
                numConsec++;
            }
            else
            {
                /*if( temp )
                { printf("Skipped!\n"); printf("i: %i, j: %i\n", i + 1, j + 1); temp = 0; }*/
                //printf("numConsec: %i\n", numConsec);
                numConsec = 0;
            }
        }
    }
    //printf("numEmpty: %i\n", numEmpty);
}

//#include <conio.h>
int CreatePuzzle( int puzzle[][9], Difficulty difficulty )
{
    int val, index, temp;

    // Initialize puzzle to zero so that any uninit cell below will be caught in solver
    ZeroInitPuzzle( puzzle );

    // Get random numbers for each of the sectors in the diagonal from top left to bottom right
    for( int k = 0; k < 3; k ++ )
    {
        int nums[9] = {1,2,3,4,5,6,7,8,9};
        int numCounter = 0;
        for( int i = 0; i < 3; i++ )
        {
            for( int j = 0; j < 3; j++, numCounter++ )
            {
                index = rand() % ( 9 - numCounter );

                val = nums[index];

                temp = nums[8 - numCounter];
                nums[index] = temp;

                puzzle[i][j] = val;
            }
        }
    }

    // Solve the rest of the puzzle
    SudokuSolver( puzzle, 0, 0 );


    //printf("Original Puzzle: \n");
    //PrintPuzzle( puzzle ); printf("\n\n\n");

    EmptySomeCells( puzzle, difficulty );

    //printf("Emptied Puzzle: \n");
    //PrintPuzzle( puzzle ); printf("\n\n\n");

    //printf("Resolved Puzzle: \n");
    //SudokuSolver( puzzle, 0, 0 );
    //PrintPuzzle( puzzle ); printf("\n\n\n");

    //getch();

    return 0;
}

void EmptyPuzzle( int puzzle[][9] )
{
    ZeroInitPuzzle( puzzle );
}
