

#include <windows.h>
#include <stdio.h>
#include <ctype.h>

#include "userinterface.h"



int Setup()
{
    //hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

    SetConsoleTitle("Soduku v1.0.1");

    system("cls");

    printf("Welcome to Soduku!\n");

    int choice;
    int valid = 0;

    while( !valid )
    {
        printf("Choose what you wish to do from the options below.\n");
        printf("1 > Create a Printable Puzzle.\n");
        printf("2 > Solve a Puzzle.\n");
        printf("3 > Play Sudoku!\n");
        printf("4 > Exit\n");

        printf("Choice: ");
        fflush(stdin);
        scanf("%i", &choice);

        if( choice == 1 || choice == 2 || choice == 3 || choice == 4 )
        { valid = 1; }
        else
        { system("cls"); printf("Invalid Choice! Try Again.\n\n"); }
    }

    system("cls");
    printf("\n");

    switch(choice)
    {
        case 1:
        {
            #ifdef DEBUG
                printf("Player chose to Create a Printable Puzzle...\n");
            #endif

            return 1;
        } break;

        case 2:
        {
            #ifdef DEBUG
                printf("Player chose to Solve a Puzzle...\n");
            #endif
            return 2;
        } break;

        case 4:
        {
            #ifdef DEBUG
                printf("Player chose to Exit...\n");
            #endif
            return 4;
        }

        default:
        {
            #ifdef DEBUG
                printf("Player chose to Play Sudoku...\n");
            #endif
            return 0;
        } break;
    }
}


int DisplayGameMenu()
{
    int choice;
    int valid = 0;

    while( !valid )
    {
        printf("Please select from the game options below:\n");
        printf("1 > New Game\n");
        printf("2 > Load Game\n");
        printf("3 > Settings\n");
        printf("4 > Quit Game\n");

        printf("Choice: ");
        fflush(stdin);
        scanf("%i", &choice);

        if( choice == 1 || choice == 2 || choice == 3 || choice == 4 )
        { valid = 1; }
        else
        { system("cls"); printf("Invalid Choice! Try Again.\n\n"); }
    }

    system("cls");
    printf("\n\n\n");

    return choice;
}

// TODO, FINISH... NOT IMPLEMENTED INTO GAME ANYMORE
void SetConsoleSize()
{
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    SMALL_RECT* srWindow = &consoleInfo.srWindow;

     // TODO: Use configuration file instead?
    printf("Please choose the size of window for your game\n");
    printf("1 > Small\n");
    printf("2 > Medium\n");
    printf("3 > Large\n");
    printf("4 > Full Screen\n");
    int choice;
    char choiceC;
    while( choice != 1 && choice != 2 && choice != 3 && choice != 4 )
    {
		fflush(stdin);
        printf("Your Choice (1,2,3,4): ");
        scanf("%c", &choiceC);
        choice = choiceC - '0';
    }

    unsigned long dwWidth = GetSystemMetrics(SM_CXSCREEN);
    unsigned long dwHeight = GetSystemMetrics(SM_CYSCREEN);

    switch( choice )
    {
        case 1:
        {
            // TODO! Need to figure out how to make this work in terms of characters...
            srWindow->Left = dwWidth / 5;
            srWindow->Top = dwHeight / 4;
            srWindow->Right = dwWidth - srWindow->Left;
            srWindow->Bottom = dwHeight - srWindow->Top;
        } break;

        case 2:
        {
            srWindow->Left = 0;
            srWindow->Top = 0;
            srWindow->Right = 100;
            srWindow->Bottom = 20;
        } break;

        case 3:
        {
            srWindow->Left = 0;
            srWindow->Top = 0;
            srWindow->Right = 100;
            srWindow->Bottom = 20;
        } break;

        case 4:
        {
            srWindow->Left = 0;
            srWindow->Top = 0;
            srWindow->Right = 100;
            srWindow->Bottom = 20;
        } break;
    }

    SetConsoleWindowInfo( hStdout, TRUE, &consoleInfo.srWindow );

    if( !GetConsoleScreenBufferInfo( hStdout, &consoleInfo ) )
    {
        exit(0);
    }

    // Print out a divider line
    for( int i = 0; i < consoleInfo.dwSize.X; i++ )
    {
        printf("_");
    }
}
