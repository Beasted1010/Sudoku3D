//#define _WIN32_WINNT_WS08 0x0600
//#define _WIN32_WINNT 0x0500

#include <windows.h>
#include <wincon.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>

#include "board.h"
#include "config.h"
#include "solver.h"
#include "userinterface.h"
#include "sudoku_game.h"


// To allow for portability
// __attribute__ ((unused)) is to indicate that a variable is not to be used and the corresponding
//warnings are suppressed
#ifdef __GNUC__
#define VARIABLE_IS_NOT_USED __attribute__ ((unused))
#else
#define VARIABLE_IS_NOT_USED
#endif


// TODO: Add a game timer to allow for high scores to be broken
// TODO: Option to save a puzzle with user added note to it
// TODO: Option to turn on/off Row/Col number indicators for config files

// TODO: Make solution.txt print out original (user inserted) board in grid format

// BEFORE RELEASE TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
// TODO: Test Difficulties and ensure they are playable.
// -> I noticed on 7 an 8 level that a puzzle came up where it allowed multiple same number entries
// -> This seemed to become a problem when whole sectors of values were omitted from the puzzle during creation

/* TODO:
NOTE: The SudokuSolver seems to return 1 when the value user enters exists in the same row, col, and block
ValidNum seems to catch that the value does not belong
I need to ensure my understanding of SudokuSolver and ensure that I can use it as a check for solvability
*/

//const char* truthInTheStones = "Truth_in_the_Stones";
//const char* dooblyDoo = "DooblyDoo";

Board* board;

void PlaySudoku( Board* board, FILE* fp );

int main()
{
    HINSTANCE hInstance;

    hInstance = GetModuleHandle(NULL);

    WNDCLASS VARIABLE_IS_NOT_USED window_class = {0};
    window_class.hIcon = (HICON) LoadImage( hInstance, MAKEINTRESOURCE( 101 ), IMAGE_ICON, 0, 0,
                                                                    LR_DEFAULTCOLOR | LR_DEFAULTSIZE );

    srand(time(NULL));

    board = malloc( sizeof(Board) );
    InitializeSettings( board );


    FILE* fp = OpenConfigFile( settingsConfigFile, "r+" );
    WriteSettingsConfig( board, fp, settingsConfigFile );
    CloseConfigFile( fp );


    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    WORD saved_attributes;

    hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

    GetConsoleScreenBufferInfo( hStdout, &consoleInfo );
    saved_attributes = consoleInfo.wAttributes;


    char prevSound[128];
    char music[256];


    // Application loop
    while( 1 )
    {
        FILE* sfp = OpenConfigFile( settingsConfigFile, "r" );
        if( !ReadSettingsConfig( board, sfp ) )
        {
            printf("Please Check \"%s\" Configuration File.\n", settingsConfigFile);
            printf("Terminating Program!\n");
            exit(0);
        }
        CloseConfigFile( sfp );

        if( !strcmp(board->settings.music, "NONE") )
        { PlaySoundA( NULL, NULL, SND_LOOP | SND_ASYNC ); }
        else if( strcmp(board->settings.music, prevSound) )
        {
            strcpy( music, path );
            strcat( music, board->settings.music );
            strcat( music, ".wav" );
            PlaySoundA( music, NULL, SND_LOOP | SND_ASYNC );
        }
        strcpy( prevSound, board->settings.music );

        SetConsoleColor( board->settings.appTextColor, FOREGROUND );
        // NOTE: Setting the color of the text without specifying the background color sets the background color
        //to 0 (black). So whenever we call the Windows console color function we are actually resetting background
        //to black for the text being printed out. Need to allow these values to be merged in some way, or at the
        //very least get rid of the background color option in config and in code
        //SetConsoleColor( board->settings.appBackgroundColor, BACKGROUND ); //NOTE: This is not what I expected

        switch( Setup() )
        {
            case 1: // Create a Puzzle
            {
                #ifdef DEBUG
                    printf("Creating Puzzle...\n\n");
                #endif

                int difficulty = 0;
                int choice = 0;
                char choiceC;

                printf("Select the difficulty for the puzzle\n");
                printf("1 > EASY\n");
                printf("2 > MEDIUM\n");
                printf("3 > HARD\n");
                printf("4 > EXTREME\n");

                while( choice != 1 && choice != 2 && choice != 3 && choice != 4 )
                {
                    printf("Your Choice (1,2,3,4): ");
                    scanf("%c", &choiceC);
                    choice = choiceC - '0';
                }

                switch( choice )
                {
                    case 1:
                    {
                        difficulty = EASY;
                        printf("\nPuzzle difficulty is EASY...\n\n");
                    } break;
                    case 2:
                    {
                        difficulty = MEDIUM;
                        printf("\nPuzzle difficulty is MEDIUM...\n\n");
                    } break;
                    case 3:
                    {
                        difficulty = HARD;
                        printf("\nPuzzle difficulty is HARD...\n\n");
                    } break;
                    case 4:
                    {
                        difficulty = EXTREME;
                        printf("\nPuzzle difficulty is EXTREME...\n\n");
                    } break;

                    default:
                    {
                        printf("Error in Difficulty Setting for Create a Puzzle\n");
                        printf("Terminating Application\n");
                        exit(0);
                    }
                }

                FILE* fp = OpenConfigFile( puzzleConfigFile, "w+" );

                CreatePuzzle( board->puzzle, difficulty );
                InitConfig( board->puzzle, fp );

                SudokuSolver( board->puzzle, 0, 0 );

                UpdateConfig( board->puzzle, fp );

                CloseConfigFile( fp );

                printf("Puzzle created, see \"%s\" for the puzzle and solution\n", puzzleConfigFile);

                SetConsoleTextAttribute( hStdout, saved_attributes );
            } break;

            case 2: // Solve a Puzzle
            {
                #ifdef DEBUG
                    printf("Reading Puzzle...\n\n");
                #endif

                FILE* fp = OpenConfigFile( solverConfigFile, "r+" );

                if( !ValidConfigFile( solverConfigFile ) )
                {
                    printf("Please Check \"%s\" Configuration File.\n", solverConfigFile);
                    printf("Terminating Program!\n");
                    exit(0);
                }

                ReadPuzzle( board->puzzle, fp );

                SudokuSolver( board->puzzle, 0, 0 );

                UpdateConfig( board->puzzle, fp );

                CloseConfigFile( fp );

                printf("Puzzle solved, see \"%s\" for the puzzle and solution\n", solverConfigFile);

                SetConsoleTextAttribute( hStdout, saved_attributes );
            } break;

            case 4: // Quit Game
            {
                #ifdef DEBUG
                    printf("Closing Application...\n\n");
                #endif

                printf("Goodbye!\n");

                SetConsoleTextAttribute( hStdout, saved_attributes );
                exit(0);
            } break;

            default: // case 3: // Play Sudoku
            {
                #ifdef DEBUG
                    printf("Playing Sudoku...\n\n");
                #endif

                FILE* fp = OpenConfigFile( savesConfigFile, "r+" );

                PlaySudoku( board, fp );

                CloseConfigFile( fp );

                SetConsoleTextAttribute( hStdout, saved_attributes );
            } break;
        }

        printf("\nPress any key to return to Application Main Menu\n");
        getch();
    }

    SetConsoleTextAttribute( hStdout, saved_attributes );
    return 0;
}
